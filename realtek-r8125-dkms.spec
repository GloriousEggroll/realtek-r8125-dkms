%define module r8125
%define pkgname realtek-r8125

Summary: Reaktek r8125 2.5 GbE kernel module (dkms)
Name: %{pkgname}-dkms
Version: 9.003.05
# Release Start
Release: 1%{?dist}
# Release End
License: GPLv2+
URL: https://www.realtek.com/en/component/zoo/category/network-interface-controllers-10-100-1000m-gigabit-ethernet-pci-express-software
Vendor: Realtek
Group: System Environment/Kernel
BuildArch: noarch
Requires: dkms bash binutils gcc kmod kernel-devel
Source: %{pkgname}-%{version}.tar.bz2
Provides: kmod-r8125 = %{version} kmod(r8125.ko) = %{version}

%description
A dkms module for the Realtek r8125 2.5GbE driver.

Support for these cards will be added to the r8169 driver in kernel 5.9, but for
earlier kernels, there is this driver.

%prep
%setup -n realtek-r8125 -q

%build

%install
mkdir -p %{buildroot}/usr/src/%{module}-%{version}/
cp -r src/. %{buildroot}/usr/src/%{module}-%{version}
cat <<'EOF' > %{buildroot}/usr/src/%{module}-%{version}/dkms.conf
PACKAGE_NAME="%{pkgname}"
PACKAGE_VERSION="%{version}"
MAKE="'make' -j`nproc` KVER=${kernelver} KERNELDIR=${kernel_source_dir} BASEDIR=${kernel_source_dir}/.. modules"
CLEAN="make clean"
BUILT_MODULE_NAME[0]="%{module}"
BUILT_MODULE_LOCATION[0]=""
DEST_MODULE_LOCATION[0]="/kernel/drivers/net/ethernet/realtek"
AUTOINSTALL="yes"
REMAKE_INITRD=no
EOF

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root)
%attr(0755,root,root) /usr/src/%{module}-%{version}/

%post
if ! /usr/sbin/dkms status %{module}/%{version} | grep -q added
then
    /usr/sbin/dkms add -m %{module} -v %{version}
fi
/usr/sbin/dkms build -m %{module} -v %{version}
/usr/sbin/dkms install -m %{module} -v %{version}
exit 0

%preun
/usr/sbin/dkms remove -m %{module} -v %{version} --all
exit 0

%changelog
* Wed Oct 07 2020 Trent Piepho <tpiepho@gmail.com> 9.003.05-1dkms
- Initial version
